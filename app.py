import os
from flask import Flask, flash, request, redirect, url_for
from werkzeug.utils import secure_filename
from flask import send_from_directory
import datetime
import torch
from detect import run
from flask import send_file

cur_time = datetime.datetime.now().strftime("%Y%m%d%H%M%S")

UPLOAD_FOLDER = 'static'
ALLOWED_EXTENSIONS = {'png', 'jpg', 'jpeg', 'mp4'}

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


@app.route('/', methods=['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']

        # if user does not select file, browser also
        # submit an empty part without filename
        if file.filename == '':
            flash('No selected file1')
            return redirect(request.url)
        if (file and allowed_file(file.filename)):
            filename1 = secure_filename(file.filename)
            file1_path = os.path.join(app.config['UPLOAD_FOLDER'], cur_time + '-'+ filename1)
            file.save(file1_path)
            save_dir = run(source=file1_path)
            return send_file(os.path.join(save_dir,cur_time + '-'+ filename1), as_attachment=True)
            
    return '''

    
    <!doctype html>
    <title>Face Mask Detection</title>
    <h1>Face Mask Detection</h1>
    <form method=post enctype=multipart/form-data>
      Input Image/Video: <br>
      <input type=file name=file><br><br>
      <input type=submit value=Detect><br>
    </form>
    '''

if  __name__ == '__main__':
    app.run(host="0.0.0.0",debug=True)